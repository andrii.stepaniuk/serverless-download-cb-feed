import os
from datetime import datetime

import boto3
import requests


urls = {
    'v1': 'https://accounts.clickbank.com/feeds/marketplace_feed_v1.xml.zip',
    'v2': 'https://accounts.clickbank.com/feeds/marketplace_feed_v2.xml.zip'
}

OUTPUT_BUCKET = os.getenv('OUTPUT_BUCKET')
DATE_FORMAT = os.getenv('DATE_FORMAT')


def handle(event, context):
    date_str = datetime.utcnow().date().strftime(DATE_FORMAT)
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(OUTPUT_BUCKET)
    for version in urls:
        version_url = urls[version]
        response = requests.get(version_url)
        bucket.put_object(Body=response.content, Key=f'{date_str}_marketplace_feed_{version}.xml.zip')
