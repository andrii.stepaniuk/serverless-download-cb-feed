#### Description:  
[Serverless](https://github.com/serverless/serverless) 
service to download [ClickBank](https://www.clickbank.com/) 
[Feed](https://support.clickbank.com/hc/en-us/articles/220376547-Marketplace-Feed)
and put it to AWS S3 bucket.

#### Prerequisites:  
[Serverless Python Requirements Plugin](https://www.npmjs.com/package/serverless-python-requirements) should be installed  
```
sls plugin install -n serverless-python-requirements
```

#### Usage:  
##### To deploy:
```
OUTPUT_BUCKET=[your S3 bucket] serverless deploy
```

##### To invoke:
```
serverless invoke -f download --log
```